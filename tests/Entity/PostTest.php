<?php

namespace App\Tests\Entity;

use App\Entity\Post;
use PHPUnit\Framework\TestCase;


class PostTest extends TestCase
{
    public function testPostTitle(): void
    {
        $comment = new Post();
        $comment->setTitle('Post title');
        $this->assertSame('Post title', $comment->getTitle());
    }
}
