<?php

namespace App\Tests\Entity;

use App\Entity\Comment;
use PHPUnit\Framework\TestCase;

class CommentTest extends TestCase
{
    public function testCommentContent(): void
    {
        $comment = new Comment();
        $comment->setContent('Comment content');
        $this->assertSame('Comment content', $comment->getContent());
    }
}
